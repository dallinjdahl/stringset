#include "stringset.h"

int in_x(char c, char *s) {
	if(!s) return 0;
	while(*s) {
		if (c == *s) return 1;
		s++;
	}
	return 0;
}

int in_n(char c, char *s) {
	if(!s) return 0;
	while(s[0] && s[1]) {
		if (s[0] <= c && c <= s[1]) return 1;
		s++;
	}
	return 0;
}
