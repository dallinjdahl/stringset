//the suffix x implies the set is defined extensionally, enumerating
//all cases.  An example matching all hexadecimal digits:
//  "abcdefABCDEF0123456789"
int in_x(char c, char *s);

//the suffix n implies the set is defined intensionally, specifying
//ranges.  An example matching all hexadecimal digits: "afAF09"
int in_n(char c, char *s);
